-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
--
-- créer un service forum rapide convivial et minimaliste
-- juste un système de tag de topic et de message avec des user et un peu d'administration
-- Table : users, discussions, messages, tags
--
-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

CREATE TABLE users (         -- Liste des utilisateurs
  id          BIGINT          unsigned    NOT NULL              AUTO_INCREMENT,
  login       VARCHAR(20)                 NOT NULL                            ,
  pseudo      VARCHAR(20)                 NOT NULL                            ,
  email       VARCHAR(255)                NOT NULL                            ,
  mdp         VARCHAR(535)                NOT NULL                            ,
  role        VARCHAR(20)                 NOT NULL                            ,
  created     DATETIME                    NOT NULL DEFAULT   CURRENT_TIMESTAMP,
  modified    DATETIME                    NOT NULL DEFAULT   CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  -- contraintes
  PRIMARY KEY (id),
  UNIQUE KEY (login, pseudo, email)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE tags (          -- Catégories (repere visuel + critere de recherche)
  id          BIGINT          unsigned            NOT NULL            AUTO_INCREMENT,
  nom         VARCHAR(30)                         NOT NULL                          ,
  role        VARCHAR(20)                         NOT NULL                          ,
  -- contraintes
  PRIMARY KEY (id),
  UNIQUE KEY (nom)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

INSERT INTO `tags` (`id`, `nom`, `role`) VALUES
  (1, 'Important', 'admin'),
  (2, 'Informatique', 'user'),
  (3, 'Loisir', 'user');

CREATE TABLE discussions (   -- Les discussions
  id          BIGINT          unsigned            NOT NULL              AUTO_INCREMENT,
  user_id     BIGINT          unsigned            NOT NULL                            ,
  tag_id      BIGINT          unsigned            NOT NULL                            ,
  titre       VARCHAR(255)                        NOT NULL                            ,
  created     DATETIME                    NOT NULL DEFAULT   CURRENT_TIMESTAMP,
  modified    DATETIME                    NOT NULL DEFAULT   CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  -- contraintes
  PRIMARY KEY (id),
  FOREIGN KEY(user_id) REFERENCES users(id),
  FOREIGN KEY(tag_id) REFERENCES tags(id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE messages (   -- Les messages
  id            BIGINT          unsigned            NOT NULL           AUTO_INCREMENT   ,
  user_id       BIGINT          unsigned            NOT NULL                            ,
  discussion_id BIGINT          unsigned            NOT NULL                            ,
  message       TEXT                                NOT NULL                            ,
  created     DATETIME                    NOT NULL DEFAULT   CURRENT_TIMESTAMP,
  modified    DATETIME                    NOT NULL DEFAULT   CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  -- contraintes
  PRIMARY KEY (id),
  FOREIGN KEY(user_id) REFERENCES users(id),
  FOREIGN KEY(discussion_id) REFERENCES discussions(id)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

-- ======================================================================
--
--  Création des vues
--  Permet d'avoir des requetes SELECT toute prete
--  aux quelles on prend ce que l'on veut
--  ainsi obtenir les attributs d'une table via une clé étrangère
--
-- ======================================================================

# CREATE VIEW messagesEnhanced (
#     id_mes, id_aut_mes, id_disc_mes, message,
#     pseudo_mes, email_mes,
#     titre, id_aut_disc,
#     pseudo_disc, email_disc,
#     tag
# )
# AS SELECT
#      messages.id, messages.auteur, messages.discussion, messages.message,
#      userMes.pseudo, userMes.email,
#      discussions.titre, discussions.auteur,
#      userDisc.pseudo, userDisc.email,
#      tags.nom
#    FROM messages, users userMes, discussions, users userDisc, tags
#    WHERE messages.auteur = userMes.id
#          AND messages.discussion = discussions.id
#          AND discussions.auteur = userDisc.id
#          AND discussions.tag = tags.id ;
#
#
# CREATE VIEW discussionsEnhanced (
#     id_disc, id_auteur_disc, id_tag_disc, titre,
#     pseudo_auteur, email_auteur,
#     nom_tag
# )
# AS SELECT
#      discussions.id, discussions.auteur, discussions.tag, discussions.titre,
#      users.pseudo, users.email,
#      tags.nom
#    FROM discussions, users, tags
#    WHERE discussions.auteur = users.id
#          AND discussions.tag = tags.id;