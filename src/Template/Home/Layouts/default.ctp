<!DOCTYPE html>
<html lang="fr">
<?= $this->element('../Home/Element/head') ?>
<body>
    <?= $this->element('../Home/Element/header') ?>

    <section class="container">
        <?= $this->fetch('content'); ?>
    </section>

    <?= $this->element('../Home/Element/footer') ?>
</body>
</html>