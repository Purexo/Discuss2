<?php  $this->extend('/Home/Layouts/default'); ?>

<?php foreach($users as $user): ?>
<hr>
<article class="row">
    <header class="col-sm-9">
        <h1><?= $this->Html->link(
            $user->pseudo,
            ['controller' => 'User', 'action' => 'oneUserById', $user->id]);
        ?></h1>
    </header>
    <aside class="col-sm-3">
        <div class="row"><?= $this->Html->link(
                'Discussions',
                ['controller' => 'Discussion', 'action' => 'allByUser', $user->id]);
            ?></div>
        <div class="row"><?= $this->Html->link(
                'Messages',
                ['controller' => 'Message', 'action' => 'allByUser', $user->id]);
            ?></div>
        <div class="row">Created : <?= $user->created ?></div>
        <div class="row">Modified : <?= $user->modified ?></div>
    </aside>
</article>
<?php endforeach; ?>