<?php
$this->extend('/Home/Layouts/default');
?>

<header class="row">
    <h1 class="col-sm-9"><?= $this->Html->link(
            $discussion->titre,
            ['controller' => 'Discussion', 'action' => 'oneDiscussion', $discussion->id, $discussion->titre]);
        ?></h1>
    <aside class="col-sm-3">
        <div class="row">By : <?= $this->Html->link(
                $discussion->user->pseudo,
                ['controller' => 'User', 'action' => 'oneUserById', $discussion->user->id]);
            ?></div>
        <div class="row"><?= $discussion->tag->nom ?></div>
        <div class="row">Created : <?= $discussion->created ?></div>
        <div class="row">Modified : <?= $discussion->modified ?></div>
    </aside>
</header>

<?php foreach ($discussion->messages as $message): ?>
    <hr>
    <article class="message" id="message-<?= $message->id ?>">
        <header class="row message-header">
            <span class="col-sm-9"><?= $this->Html->link(
                    $message->user->pseudo,
                    ['controller' => 'User', 'action' => 'oneUserById', $message->user->id]);
                ?></span>
            <aside class="col-sm-3">
                <div class="row"><a href="#message-<?= $message->id ?>">#permalink</a></div>
                <div class="row">Created : <?= $message->created ?></div>
                <div class="row">Modified : <?= $message->modified ?></div>
            </aside>
        </header>
        <div class="message-content">
            <?= $message->message ?>
        </div>
    </article>
<?php endforeach; ?>
