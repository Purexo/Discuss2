<?php  $this->extend('/Home/Layouts/default'); ?>

<div class="jumbotron">
    <h1>Hello, world!</h1>
    <p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a></p>
</div>

<?php foreach($discussions as $discussion): ?>
<article class="row">
    <header class="col-sm-9">
        <h1><?= $this->Html->link(
            $discussion->titre,
            ['controller' => 'Discussion', 'action' => 'oneDiscussion', $discussion->id, $discussion->titre]);
        ?></h1>
    </header>
    <aside class="col-sm-3">
        <div class="row"><?= $discussion->tag->nom ?></div>
        <div class="row">By : <?= $this->Html->link(
                $discussion->user->pseudo,
                ['controller' => 'User', 'action' => 'oneUserById', $discussion->user->id]);
            ?></div>
        <div class="row">Created : <?= $discussion->created ?></div>
        <div class="row">Modified : <?= $discussion->modified ?></div>
    </aside>
</article>
<?php endforeach; ?>