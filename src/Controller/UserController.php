<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\ORM\TableRegistry;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class UserController extends AppController {

    public function index() {
        $users = TableRegistry::get('Users')->find('all');

        $this->set('users', $users);
        $this->render('/Home/Pages/utilisateurs');
    }

    public function oneUserById($id) {
        $user = TableRegistry::get('Users')->get($id);

        $this->set('user', $user);
        $this->render('/Home/Pages/utilisateur');
    }

    public function oneUserByPseudo($pseudo) {
        $user = TableRegistry::get('Users')->find('all')
            ->where(['Users.pseudo =' => $pseudo])
            ->limit(1)
            ->first();

        $this->set('user', $user);
        $this->render('/Home/Pages/utilisateur');
    }

}
