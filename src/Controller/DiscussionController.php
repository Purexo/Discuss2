<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\ORM\TableRegistry;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class DiscussionController extends AppController {

    public function oneDiscussion($id, $slug) {
        $discussion = TableRegistry::get('Discussions')->get($id, ['contain' => [
            'Users', 'Tags', 'Messages.Users'
        ]]);

        $this->set('discussion', $discussion);
        $this->render('/Home/Pages/discussion');
    }

    public function allByUser($id) {
        $discussions = TableRegistry::get('Discussions')->find('all', ['contain' => [
            'Users', 'Tags', 'Messages.Users'
        ]])->where(['Users.id =' => $id]);

        $this->set('discussions', $discussions);
        $this->render('/Home/Pages/default');
    }

}
